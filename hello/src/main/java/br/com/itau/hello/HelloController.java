package br.com.itau.hello;

import javax.annotation.Generated;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	@GetMapping("/ola")
	public String mostrarOla() {
		String texto =  "<h1>Ola Mundo</h1>";
		texto += "<a href='/logo'>Logo</a>";
			 return texto;
	}
	
	@GetMapping ("/logo")
	public String mostraAte() {
	return "<h1>Até logo</h1>";	

	}

}
