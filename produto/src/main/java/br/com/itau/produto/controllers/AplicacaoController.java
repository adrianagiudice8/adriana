package br.com.itau.produto.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.produto.services.AplicacaoService;
import br.com.itau.produto.viewobjects.Investimento;

@RestController
@RequestMapping("/aplicacao")
public class AplicacaoController {
	@Autowired
	AplicacaoService aplicacaoService;
	
	
	@PostMapping
	public ResponseEntity criar(@RequestBody Investimento investimento){
		Optional<Investimento> investimentoOptional = aplicacaoService.criar(investimento);
		
		if(investimentoOptional.isPresent()) {
			return ResponseEntity.status(201).body(investimentoOptional.get().getAplicacao());
		}
		
		return ResponseEntity.badRequest().build();
	}
}
