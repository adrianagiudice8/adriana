package br.com.itau.pinterest.models;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Perfil {

	@PrimaryKey
	private String email;

	@NotBlank 
	private String nome;
	
	@NotBlank 
	private String sexo;
	
	@NotBlank 
	private LocalDate dataNascimento; 

	@NotNull 
	private String preferencias;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public LocalDate getDataNAscimento() {
		return dataNascimento;
	}

	public void setDataNAscimento(LocalDate dataNAscimento) {
		this.dataNascimento = dataNAscimento;
	}
	@NotBlank 
	private String dataNascimento1;
	
	
	public String getPreferencias() {
		return preferencias;
	}

	public void setPreferencias(String preferencias) {
		this.preferencias = preferencias;
	}
	
	
	
}
	
	
	