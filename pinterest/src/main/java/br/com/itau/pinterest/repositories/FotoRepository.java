
package br.com.itau.pinterest.repositories;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import br.com.itau.pinterest.models.Foto;

public interface FotoRepository extends CrudRepository<Foto, UUID> {
}
