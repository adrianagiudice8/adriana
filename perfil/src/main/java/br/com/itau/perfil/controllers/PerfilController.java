package br.com.itau.perfil.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.perfil.Perfil;

@RestController
public class PerfilController {
	
	@GetMapping
	public Perfil exibir() {
		Perfil perfil = new Perfil();
		
		perfil.setNome("dri");
		perfil.setEmail("dri@");
		perfil.setSenha("senha");
		
		return perfil;
	}
}