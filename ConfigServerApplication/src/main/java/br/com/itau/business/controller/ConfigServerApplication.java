package br.com.itau.business.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration

public class ConfigServerApplication {
	
public static void main(String[] args) {
	
	SpringApplication.run(ConfigServerApplication.class, args);
}
}